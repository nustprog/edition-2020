# NUST Programming Competition -- Edition 2020

This repository contains all information related to the NUST programming competition edition 2020.

## The competition

This year's edition includes a programming competition for high schools as well as a hackathon for university teams.

Will present the teams and more details...

## Judges

* High school competition
	* Hienrich Aluvilu (First National Bank)
	* Jacodia Fransman (University of Namibia)
	* Arpit Jain (Namibia University of Science and Technology)
	* Anton Lungameni (University of Namibia)
* Hackathon
	* Jesaya Malwa (MTC)
	* Veijo Shikongo (Namibia Statistics Agency)
	* Albert Katjvirue (Debmarine)
	* Mateus Sakaria (Debmarine)

## Organising Team

* Herman Kandjimi
* Gereon Kapuire
* Loini Liyambo
* Jovita Mateus
* Eleazar Mbaeva
* Ndinelago Nashandi
* José Quenum
* Albertina Shilongo
* Steven Tjiraso
